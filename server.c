/*************************************************************
	>File Name: server.c
	>Author: Yue
	>Created Time: Sat 29 Nov 2014 05:08:11 PM CST
	>Description : Distributed file system
*************************************************************/

#include "udp.h"
#include "mfs.h"

#define INODE_POS(x)  lseek(fd,MFS_BLOCK_SIZE*2+(x)*64,SEEK_SET)
#define SB_POS        lseek(fd,MFS_BLOCK_SIZE,SEEK_SET);
#define BIT_MAP(x)    lseek(fd,MFS_BLOCK_SIZE*3+(x)*4,SEEK_SET)
#define DATA_POS(x)   lseek(fd,x,SEEK_SET)
#define POINTER(x,y)  lseek(fd,(x)+(y)*4,SEEK_SET)

MFS_superblock_t SB;
MFS_Inode_t	Inode[64];
int BitMap[1024],fd;
char buffer[BUFFER_SIZE],reply[BUFFER_SIZE];

void 
getargs(int *port, char *file_image,int argc, char *argv[])
{
	if (argc!=3) {
		fprintf(stderr,"Usage: %s [portnum] [file-system-image]",argv[0]);
		exit(1);
	}

	*port=atoi(argv[1]);
	strcpy(file_image,argv[2]);
}
int
Alloc_Inode()
{
	int i;
	for (i=0;i<SB.ninodes;i++)
		if (Inode[i].type==0) return i;
	return -1;
}   
int
Alloc_Block() // allocate one data block, initialize to 0
{
	int i, offset = lseek (fd, 0 , SEEK_CUR);
	for (i=0; i<SB.nblocks; i++)
		if (BitMap[i] == 0)
		{
			BitMap[i]=1;
			BIT_MAP(i);
			write(fd, &BitMap[i], 4);
			DATA_POS((i+4)*MFS_BLOCK_SIZE);
			char buf[4096];
			memset(buf, 0, MFS_BLOCK_SIZE);
			write(fd, buf, MFS_BLOCK_SIZE);
			fsync(fd);
			lseek(fd, offset, SEEK_SET);
			return (i+4)*MFS_BLOCK_SIZE;
 		}
	lseek(fd, offset, SEEK_SET);
	return -1;
} 
void
Initial_DirEnt(int pinum,int inum, int block, int Dot)
{   
	int offset=block, store = lseek(fd, 0, SEEK_CUR);
	DATA_POS(block);
	MFS_DirEnt_t m;
	if (Dot==0)
	{
		strcpy(m.name, ".");
		m.inum = inum;
		offset += write(fd, &m, 64);
		strcpy(m.name, "..");
		m.inum = pinum;
		offset += write(fd, &m, 64);
	}
	strcpy( m.name, "");
	m.inum = -1;
	while (offset < block + MFS_BLOCK_SIZE)
		offset += write (fd, &m, 64);
	lseek(fd, store, SEEK_SET);
}  
void
Img_Init()
{    
	SB.size=1028; //1 unused block, 1 superblock, 1 inode block, 1 bit map block, 1024 data blocks
	SB.nblocks=1024;
	SB.ninodes=64;
	SB_POS;
	write(fd,&SB,sizeof(SB));
	int i,j;
	for (i=0;i<1024;i++)
		BitMap[i]=0;
	BIT_MAP(0);
	write(fd,BitMap,MFS_BLOCK_SIZE);
	for (i=0;i<64;i++)
	 {
		Inode[i].type=0;
		Inode[i].size=0;
		for (j=0;j<14;j++)
			Inode[i].addrs[j]=-1;
	}
	Inode[ROOTINO].type=MFS_DIRECTORY;
	Inode[ROOTINO].size=4096;
	Inode[ROOTINO].addrs[0]=Alloc_Block();
	Initial_DirEnt(ROOTINO, ROOTINO, Inode[ROOTINO].addrs[0], 0);
	INODE_POS(ROOTINO);
	write(fd,Inode,MFS_BLOCK_SIZE);
	fsync(fd);
}   
int
Check_Dir(int pinum)
{
	int i=-1,j,offset=lseek(fd,0,SEEK_CUR);
	MFS_DirEnt_t entry;
	while (i<14 && Inode[pinum].addrs[++i]>0)
	{
		DATA_POS(Inode[pinum].addrs[i]);
		read(fd,&entry,sizeof(entry));
		if (strcmp(entry.name,".")!=0 && strcmp(entry.name,"..")!=0 && entry.inum!=-1) 
		{
			lseek(fd,offset,SEEK_SET);
			return -1; //not empty
		}
	}
	lseek(fd,offset,SEEK_SET);
	return 0; //empty
}
int
LOOKUP()
{ 
	int temp,pinum,i=-1,j;
	char name[60];

	MFS_DirEnt_t entry;
	sscanf(buffer,"%d %d %s",&temp,&pinum,name);

	if (pinum >= SB.ninodes || pinum<0 || Inode[pinum].type!=MFS_DIRECTORY) return -1;

	while(i<14 && Inode[pinum].addrs[++i]>0)
	{ 
		DATA_POS(Inode[pinum].addrs[i]);
		for (j=0;j<64;j++)
	 	{
			read(fd,&entry,sizeof(entry));
			if (entry.inum!=-1&&strcmp(entry.name,name)==0) //found this name
				return entry.inum;
		}
	} 
	return -1;
} 
void
STAT()
{
	int temp,inum;
	sscanf(buffer,"%d %d",&temp,&inum);
	if (inum < 0 || inum > SB.ninodes || Inode[inum].type==0) 
	{
		sprintf(reply,"%d", -1);
		return;
	}

	sprintf(reply,"%d", 0);
	MFS_Stat_t Stat;
	Stat.type=Inode[inum].type;
	Stat.size=Inode[inum].size;
	memcpy((void*)&reply[100],(void*)&Stat,sizeof(Stat));
} 
int
WRITE()
{ 
	int temp, inum, block, i, block_1,block_2;
	char Write_Block[MFS_BLOCK_SIZE];

	sscanf(buffer,"%d %d %d",&temp,&inum,&block);
	if (block <0 || inum < 0 || inum > SB.ninodes || Inode[inum].type != MFS_REGULAR_FILE) return -1; //invalid inum or not a regular file

	
	block_1=(block > 12)? 13 : block;
	block_2=(block > 12)? block-13 : -1;
	
	Inode[inum].size= ((block+1) * MFS_BLOCK_SIZE > Inode[inum].size) ? (block+1) * MFS_BLOCK_SIZE : Inode[inum].size;

	if ((Inode[inum].addrs[block_1]==-1) && (Inode[inum].addrs[block_1]=Alloc_Block()) == -1) return -1; //No enough space;
	INODE_POS(inum);
	write(fd, &Inode[inum], 64);
		
	if (block_2 >= 0)
	{ 
		POINTER(Inode[inum].addrs[13],block_2);
		read(fd, &block, 4);
		if ( (block == 0) && (block=Alloc_Block())==-1) return -1; //no enough space
		POINTER(Inode[inum].addrs[13],block_2);
		write(fd, &block,4);
	}
	else block=Inode[inum].addrs[block_1];

	memcpy(Write_Block,&buffer[100],MFS_BLOCK_SIZE);
	DATA_POS(block);
	write(fd,Write_Block,MFS_BLOCK_SIZE);
	fsync(fd);
	return 0;
}
void
READ()
{ 
	int temp, inum, block,i;
	char Read_Block[MFS_BLOCK_SIZE];
	sscanf(buffer,"%d %d %d",&temp, &inum, &block);
	if (inum < 0 || inum > SB.ninodes || Inode[inum].type==0) //invalid inum
		goto bad;
	if (block < 0 || (block > 13 && Inode[inum].addrs[13]==-1)) //invalid block
		goto bad;

	
 	if (block < 13)
 	{ 
		if (Inode[inum].addrs[block]==-1) 
			goto bad;
		block=Inode[inum].addrs[block];
	}
	else
 	{ 
		POINTER(Inode[inum].addrs[13],block-13);
		read(fd, &block, 4);
 		if (block == 0)
			goto bad;
 	} 

	sprintf(reply, "%d", 0);
	DATA_POS(block);
	read(fd,Read_Block,MFS_BLOCK_SIZE);
	memcpy((void*)&reply[100],(void*)Read_Block,MFS_BLOCK_SIZE);
	return;

bad: sprintf(reply, "%d", -1);
}  
int
CREAT()
{ 
	int pinum,type,temp,inum,dnum;
	char name[60];
	MFS_DirEnt_t entry;
	sscanf(buffer,"%d %d %d %s",&temp, &pinum, &type, name);
	if (pinum <0 || pinum > SB.ninodes || Inode[pinum].type != MFS_DIRECTORY) return -1;
	int i=-1,j;
	while (++i<14)
	{
		if (Inode[pinum].addrs[i]==-1) // increase directory size
		{
			if ((Inode[pinum].addrs[i]=Alloc_Block())==-1) return -1;
			Initial_DirEnt(pinum, inum, Inode[pinum].addrs[i], i);
			Inode[pinum].size+=MFS_BLOCK_SIZE;
			INODE_POS(pinum);
			write(fd,&Inode[pinum],64);
		}

		DATA_POS(Inode[pinum].addrs[i]);
		for (j=0;j<64; j++)
		{ 
			read(fd,&entry,sizeof(entry));
			if (strcmp(entry.name,name)==0 && entry.inum!=-1) return 0;
			if (entry.inum!=-1)
				continue;
			if ((inum=Alloc_Inode())<0) return -1;
			if (type==MFS_DIRECTORY &&(dnum=Alloc_Block()) == -1) return -1; // no enough space
			strcpy(entry.name,name);
			entry.inum=inum;
			lseek(fd,-64,SEEK_CUR);
			write(fd,&entry,sizeof(entry));
			
			Inode[inum].type=type;
			for (i=0;i<14;i++) Inode[inum].addrs[i]=-1;
			if (type==MFS_DIRECTORY) 
			{
				Inode[inum].addrs[0]=dnum;
				Initial_DirEnt(pinum, inum, dnum, 0);
				Inode[inum].size=MFS_BLOCK_SIZE;
			}
			INODE_POS(inum);
			write(fd,&Inode[inum],sizeof(Inode[inum]));
			fsync(fd);
			return 0;
		}
 	}
	return -1; //directory is full
}
int
UNLINK()
{
	int temp, pinum,i=-1,j;
	char name[60];

	sscanf(buffer,"%d %d %s", &temp, &pinum, name);
	if (pinum >= SB.ninodes || pinum < 0 || Inode[pinum].type!=MFS_DIRECTORY) return -1;
	
	MFS_DirEnt_t entry;
	while (i<14 && Inode[pinum].addrs[++i]!=-1)
	{
		DATA_POS(Inode[pinum].addrs[i]);
		for (j=0;j<64;j++)
		{
			read(fd,&entry,64);
			if (strcmp(entry.name,name)==0 && entry.inum!=-1) //find
			{
				if (Inode[entry.inum].type==MFS_DIRECTORY && Check_Dir(entry.inum)==-1) return -1;
				temp=entry.inum;
				entry.inum=-1;
				lseek(fd,-64,SEEK_CUR);
				write(fd,&entry,64);           //delete entry

				Inode[temp].type=0;      //release inode
				for (i=0;i<14;i++)             //release data block
					if (Inode[temp].addrs[i]!=-1)
					{
						BitMap[Inode[temp].addrs[i]/MFS_BLOCK_SIZE]=0;
						Inode[temp].addrs[i]=-1;
					}
				Inode[temp].size=0;
				INODE_POS(temp);
				write(fd,&Inode[temp],64);

				BIT_MAP(0);
				write(fd,BitMap,MFS_BLOCK_SIZE);
				fsync(fd);

				DATA_POS(Inode[pinum].addrs[i]);
				for (i=0;i<64;i++)
				{
					read(fd,&entry,64);
					if (entry.inum!=-1)
						return 0;
				}
				//release one data block
				Inode[pinum].size-=MFS_BLOCK_SIZE;
				BitMap[Inode[pinum].addrs[i]/MFS_BLOCK_SIZE]=0;
				BIT_MAP(Inode[pinum].addrs[i]/MFS_BLOCK_SIZE);
				write(fd,&BitMap[Inode[pinum].addrs[i]/MFS_BLOCK_SIZE],4);
				Inode[pinum].addrs[i]=-1;
				INODE_POS(pinum);
				write(fd,&Inode[pinum],64);
				fsync(fd);
				return 0;
			}
		}
	}
	return 0;
}
int 
main(int argc,char *argv[])
{ 
	char file_image[1024];
	int port,i,CMD;

	getargs(&port,file_image,argc,argv);

	fd=open(file_image,O_RDWR|O_CREAT|O_EXCL,0644);
	if (fd>0)
		Img_Init();
	else
	{
		fd=open(file_image,O_RDWR);
		lseek(fd,MFS_BLOCK_SIZE,SEEK_SET);
		read(fd,&SB,sizeof(SB));
		INODE_POS(0);
		for (i=0;i<SB.ninodes;i++)
			read(fd,&Inode[i],64);
		BIT_MAP(0);
		for (i=0;i<SB.nblocks;i++)
			read(fd,&BitMap[i],4);
	} 
	
	int sd = UDP_Open(port);
	assert(sd>=-1);
	while (1){
		struct sockaddr_in s;
		int rc= UDP_Read(sd, &s, buffer, BUFFER_SIZE);
		if (rc > 0)
		{
			sscanf(buffer,"%d",&CMD);
			switch (CMD)
			{
				default:
					sprintf(reply,"%d",-1);
					break;
				case LOOKUP_NO: 
					sprintf(reply,"%d",LOOKUP());
					break;
				case STAT_NO:
					STAT();
					break;
				case WRITE_NO:
					sprintf(reply,"%d",WRITE());
					break;
				case READ_NO:
					READ();
					break;
				case CREAT_NO:
					sprintf(reply,"%d",CREAT());
					break;
				case UNLINK_NO:
					sprintf(reply,"%d",UNLINK());
					break;
				case SHUTDOWN:
					fsync(fd);
					close(fd);
					sprintf(reply,"%d",0);
					UDP_Write(sd, &s, reply, BUFFER_SIZE);
					exit(0);
		 	}
			UDP_Write(sd, &s, reply, BUFFER_SIZE);
	 	}
	} 
	return 0;
}
