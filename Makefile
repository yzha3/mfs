all: server lib

server: server.c udp.c udp.h mfs.h
	gcc -o server server.c udp.c

lib: client_library.c udp.c udp.h mfs.h
	gcc -c -fpic client_library.c -Wall -Werror
	gcc -c -fpic udp.c -Wall -Werror
	gcc -shared -o libmfs.so client_library.o udp.o

clean:
	rm -f server libmfs.so client_library.o udp.o client
