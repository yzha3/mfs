Distributed File Sever
==================================================
A distributed file server based on UDP policy, which is provided by course. It stored its data in an on-disk file or the file system image. A clinet library is built as the interface to access the file server.

The on-disk file system is similar to that of xv6. The differences are:

1. Block size is 4kB

2. 13 direct pointers and 1 indirect pointer in the inode

3. Directory entries has up to 60 characters

4. No hard links

5. Up to 64 inodes and 1024 data blocks

The clinet library:

1. `int MFS_Init(char* hostname, int port)`

2. `int MFS_Lookup(int pinum, char *name)`  Success: return inode number; failure: invalid pinum, name not exist, return -1

3. `int MFS_Write(int inum, char *buffer, int block)`  Success: return 0; failure: invalid inum, invalid block, return -1

4. `int MFS_Read(int inum, char *buffer, int block)`   Success: return 0; failure: invalid inum, invalid block, return -1

5. `int MFS_Creat(int pinum, int type, char *name)`    Success: return 0; failure: pinum not exist, name is too long, return -1

6. `int MFS_Unlink(int pinum, char *name)`             Success: return 0; failure: pinum not exist, directory is not empty, return -1

7. `int MFS_Shutdown()`                                Success: return 0; failure: return -1;
