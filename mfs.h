#ifndef __MFS_h__
#define __MFS_h__

#define MFS_REGULAR_FILE (1)
#define MFS_DIRECTORY    (2)

#define MFS_BLOCK_SIZE   (4096)

#define ROOTINO 0

#define BUFFER_SIZE 4196
#define LOOKUP_NO 1
#define STAT_NO   2
#define WRITE_NO  3
#define READ_NO   4
#define CREAT_NO  5
#define UNLINK_NO 6
#define SHUTDOWN  7

typedef struct __MFS_Stat_t {
	int type;   // MFS_DIRECTORY or MFS_REGULAR
	int size;   // bytes
    // note: no permissions, access times, etc.
} MFS_Stat_t;

typedef struct __MFS_DirEnt_t {
    char name[60];  // up to 60 bytes of name in directory (including \0)
    int  inum;      // inode number of entry (-1 means entry not used)
} MFS_DirEnt_t;

typedef struct __MFS_Inode_t {
	int type;   // I_UNUSE, I_DIR or I_FILE
	int size;   // bytes;
	uint addrs[14];
} MFS_Inode_t;

typedef struct __MFS_SuperBlock_t {
	uint size;       //Size of file system image (blocks)
	uint nblocks;    //Number of data blocks
	uint ninodes;    //Number of inodes;
} MFS_superblock_t; 

int MFS_Init(char *hostname, int port);
int MFS_Lookup(int pinum, char *name);
int MFS_Stat(int inum, MFS_Stat_t *m);
int MFS_Write(int inum, char *buffer, int block);
int MFS_Read(int inum, char *buffer, int block);
int MFS_Creat(int pinum, int type, char *name);
int MFS_Unlink(int pinum, char *name);
int MFS_Shutdown();

#endif // __MFS_h__
