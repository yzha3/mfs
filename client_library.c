/*************************************************************
	>File Name: client_library.c
	>Author: Yue
	>Created Time: Sat 29 Nov 2014 07:22:43 PM CST
	>Description:
*************************************************************/

#include "udp.h"
#include "mfs.h"
#include <sys/select.h>

int sd;
struct sockaddr_in addr, addr2;
char send_message[BUFFER_SIZE];
char reply_message[BUFFER_SIZE];
struct timeval timeout;
fd_set readfds;

int
Send_Reply()
{
	int reply;
	do {
		FD_ZERO(&readfds);
		FD_SET(sd, &readfds);
		timeout.tv_sec=5;
		timeout.tv_usec=0;
		UDP_Write(sd, &addr, send_message, BUFFER_SIZE);
	}while (select(sd+1, &readfds, NULL, NULL, &timeout) <= 0);
	UDP_Read(sd, &addr2, reply_message, BUFFER_SIZE);
	sscanf(reply_message,"%d",&reply);
	return reply;
}

int
MFS_Init(char *hostname, int port)
{
	sd= UDP_Open(20000);
	if (sd<-1) return -1;
	return UDP_FillSockAddr (&addr, hostname, port);
}  

int
MFS_Lookup(int pinum, char *name)
{
	if (strlen(name)>60) return -1;
	sprintf(send_message,"%d %d %s",LOOKUP_NO,pinum,name);
 	return Send_Reply();
} 

int
MFS_Stat(int inum, MFS_Stat_t *m)
{
	sprintf(send_message,"%d %d",STAT_NO,inum);
	if (Send_Reply()==-1) return -1;
	memcpy((void*)m,(void*)&reply_message[100],sizeof(*m));
	return 0;
}   

int
MFS_Write(int inum, char *buffer, int block)
{ 
	sprintf(send_message,"%d %d %d",WRITE_NO,inum,block);
	memcpy(&send_message[100],buffer,MFS_BLOCK_SIZE);
	return Send_Reply();
} 

int
MFS_Read(int inum, char *buffer, int block)
{
	sprintf(send_message,"%d %d %d", READ_NO,inum,block);
	if (Send_Reply()==-1) return -1;
	memcpy((void*)buffer,(void*)&reply_message[100],MFS_BLOCK_SIZE);
	return 0;
}

int
MFS_Creat(int pinum, int type, char *name)
{
	if (strlen(name)>60) return -1;
	sprintf(send_message,"%d %d %d %s",CREAT_NO,pinum, type, name);
	return Send_Reply();
} 

int
MFS_Unlink(int pinum, char *name)
{
	if (strlen(name)>60) return 0;
	sprintf(send_message,"%d %d %s",UNLINK_NO, pinum, name);
	return Send_Reply();
}

int
MFS_Shutdown()
{ 
	sprintf(send_message,"%d",SHUTDOWN);
	return Send_Reply();
}
